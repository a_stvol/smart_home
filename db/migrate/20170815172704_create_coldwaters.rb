class CreateColdwaters < ActiveRecord::Migration[5.1]
  def change
    create_table :coldwaters do |t|
      t.belongs_to :communal_service, index: true
      t.integer :last_value, null: false
      t.integer :current_value, null: false
      t.decimal :price, null: false, :precision => 8, :scale => 2

      t.timestamps
    end
  end
end
