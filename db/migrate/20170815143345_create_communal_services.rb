class CreateCommunalServices < ActiveRecord::Migration[5.1]
  def change
    create_table :communal_services do |t|
      t.date :begin_period
      t.date :end_period
      t.decimal :amount, null: false, :default => 0, :precision => 8, :scale => 2

      t.timestamps
    end
  end
end
