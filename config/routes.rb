Rails.application.routes.draw do

  devise_for :users, :path => "auth", :path_names => { :sign_in => "login", :sign_out => "logout" }, :controllers => { sessions: "sessions" }
  resources :users
  
  scope module: 'communal' do
  	resources :communal_services
  	resources :coldwaters
  	resources :hotwaters
  	resources :electrics
	end

  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
