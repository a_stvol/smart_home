json.extract! coldwater, :id, :last_value, :current_value, :price, :created_at, :updated_at
json.url coldwater_url(coldwater, format: :json)
