json.extract! electric, :id, :last_value, :current_value, :price, :created_at, :updated_at
json.url electric_url(electric, format: :json)
