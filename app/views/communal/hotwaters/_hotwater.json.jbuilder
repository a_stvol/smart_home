json.extract! hotwater, :id, :last_value, :current_value, :price, :created_at, :updated_at
json.url hotwater_url(hotwater, format: :json)
