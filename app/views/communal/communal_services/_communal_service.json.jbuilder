json.extract! communal_service, :id, :period, :created_at, :updated_at
json.url communal_service_url(communal_service, format: :json)
