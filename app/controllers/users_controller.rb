class UsersController < ApplicationController

  load_and_authorize_resource
	before_action :set_user, only: [:show, :edit, :update, :destroy]


  def index
  	@users = User.all.paginate(:page => params[:page], :per_page => 10)
  end

  def show
  end

  def new
  	@user = User.new
    @roles = Role.all
  end

  def edit
  	@roles = Role.all
  end

  def create
    @roles = Role.all
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        if user_params[:role]
          roles_save(user_params[:role])
        end
        format.html { redirect_to @user, notice: 'Учетная запись успешно создана!' }
      else
        format.html { render action: 'new' }
      end
    end
  end


  def update
    @roles = Role.all
    respond_to do |format|
      if @user.update(user_params)
        if user_params[:role]
          roles_save(user_params[:role])
        end
        format.html { redirect_to @user, notice: 'Учетная запись успешно обновлена!' }
      else
        format.html { render action: 'edit' }
      end
    end
  end


  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'Учетная запись успешно удалена!' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def roles_save(roles)
      if current_user.roles.find_by_name("super_admin")
        roles.each do |role, status|
          if status == "true"
            if !@user.roles.find_by_name(role)
              if role == "active"
                if !Role.find_by_name("active").users.first
                  @user.roles << Role.find_by_name(role)
                end
              else
                @user.roles << Role.find_by_name(role)
              end
            end
          else
            if @user.roles.find_by_name(role)
              @user.roles.destroy(Role.find_by_name(role))
            end
          end
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:login, :email, :email_notification, :name, :password, :password_confirmation, :role => [:super_admin, :admin, :order_manager, :text_manager])
    end
end
