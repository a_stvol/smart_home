class Communal::HotwatersController < ApplicationController

  load_and_authorize_resource
  before_action :set_hotwater, only: [:show, :edit, :update, :destroy]

  # GET /hotwaters
  # GET /hotwaters.json
  def index
    @hotwaters = Hotwater.all
  end

  # GET /hotwaters/1
  # GET /hotwaters/1.json
  def show
  end

  # GET /hotwaters/new
  def new
    redirect_to communal_services_path if !params[:communal_service_id]
    @hotwater = Hotwater.new
    @hotwater.communal_service_id = params[:communal_service_id]
    @last_hotwater = Hotwater.last
    if @last_hotwater
      @hotwater.last_value = @last_hotwater.current_value
      @hotwater.price = @last_hotwater.price
    end
  end

  # GET /hotwaters/1/edit
  def edit
  end

  # POST /hotwaters
  # POST /hotwaters.json
  def create
    @hotwater = Hotwater.new(hotwater_params)

    respond_to do |format|
      if @hotwater.save
        format.html { redirect_to @hotwater, notice: 'Запись успешно добавлена.' }
        format.json { render :show, status: :created, location: @hotwater }
      else
        format.html { render :new }
        format.json { render json: @hotwater.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hotwaters/1
  # PATCH/PUT /hotwaters/1.json
  def update
    respond_to do |format|
      if @hotwater.update(hotwater_params)
        format.html { redirect_to @hotwater, notice: 'Запись успешно обновлена.' }
        format.json { render :show, status: :ok, location: @hotwater }
      else
        format.html { render :edit }
        format.json { render json: @hotwater.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hotwaters/1
  # DELETE /hotwaters/1.json
  def destroy
    @communal_service_id = @hotwater.communal_service.id
    @hotwater.destroy
    respond_to do |format|
      format.html { redirect_to communal_service_path(@communal_service_id), notice: 'Запись успешно удалена.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hotwater
      @hotwater = Hotwater.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hotwater_params
      params.require(:hotwater).permit(:last_value, :current_value, :price, :communal_service_id)
    end
end
