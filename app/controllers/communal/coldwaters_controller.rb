class Communal::ColdwatersController < ApplicationController

  load_and_authorize_resource
  before_action :set_coldwater, only: [:show, :edit, :update, :destroy]

  # GET /coldwaters
  # GET /coldwaters.json
  def index
    @coldwaters = Coldwater.all
  end

  # GET /coldwaters/1
  # GET /coldwaters/1.json
  def show
  end

  # GET /coldwaters/new
  def new
    redirect_to communal_services_path if !params[:communal_service_id]
    @coldwater = Coldwater.new
    @coldwater.communal_service_id = params[:communal_service_id]
    @last_coldwater = Coldwater.last
    if @last_coldwater
      @coldwater.last_value = @last_coldwater.current_value
      @coldwater.price = @last_coldwater.price
    end
  end

  # GET /coldwaters/1/edit
  def edit
  end

  # POST /coldwaters
  # POST /coldwaters.json
  def create
    @coldwater = Coldwater.new(coldwater_params)

    respond_to do |format|
      if @coldwater.save
        format.html { redirect_to @coldwater, notice: 'Запись успешно добавлена.' }
        format.json { render :show, status: :created, location: @coldwater }
      else
        format.html { render :new }
        format.json { render json: @coldwater.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /coldwaters/1
  # PATCH/PUT /coldwaters/1.json
  def update
    respond_to do |format|
      if @coldwater.update(coldwater_params)
        format.html { redirect_to @coldwater, notice: 'Запись успешно обновлена.' }
        format.json { render :show, status: :ok, location: @coldwater }
      else
        format.html { render :edit }
        format.json { render json: @coldwater.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coldwaters/1
  # DELETE /coldwaters/1.json
  def destroy
    @communal_service_id = @coldwater.communal_service.id
    @coldwater.destroy
    respond_to do |format|
      format.html { redirect_to communal_service_path(@communal_service_id), notice: 'Запись успешно удалена.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coldwater
      @coldwater = Coldwater.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def coldwater_params
      params.require(:coldwater).permit(:last_value, :current_value, :price, :communal_service_id)
    end
end
