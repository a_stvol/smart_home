class Communal::ElectricsController < ApplicationController

  load_and_authorize_resource
  before_action :set_electric, only: [:show, :edit, :update, :destroy]

  # GET /electrics
  # GET /electrics.json
  def index
    @electrics = Electric.all
  end

  # GET /electrics/1
  # GET /electrics/1.json
  def show
  end

  # GET /electrics/new
  def new
    redirect_to communal_services_path if !params[:communal_service_id]
    @electric = Electric.new
    @electric.communal_service_id = params[:communal_service_id]
    @last_electric = Electric.last
    if @last_electric
      @electric.last_value = @last_electric.current_value
      @electric.price = @last_electric.price
    end
  end

  # GET /electrics/1/edit
  def edit
  end

  # POST /electrics
  # POST /electrics.json
  def create
    @electric = Electric.new(electric_params)

    respond_to do |format|
      if @electric.save
        format.html { redirect_to @electric, notice: 'Запись успешно добавлена.' }
        format.json { render :show, status: :created, location: @electric }
      else
        format.html { render :new }
        format.json { render json: @electric.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /electrics/1
  # PATCH/PUT /electrics/1.json
  def update
    respond_to do |format|
      if @electric.update(electric_params)
        format.html { redirect_to @electric, notice: 'Запись успешно обновлена.' }
        format.json { render :show, status: :ok, location: @electric }
      else
        format.html { render :edit }
        format.json { render json: @electric.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /electrics/1
  # DELETE /electrics/1.json
  def destroy
    @communal_service_id = @electric.communal_service.id
    @electric.destroy
    respond_to do |format|
      format.html { redirect_to communal_service_path(@communal_service_id), notice: 'Запись успешно удалена.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_electric
      @electric = Electric.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def electric_params
      params.require(:electric).permit(:last_value, :current_value, :price, :communal_service_id)
    end
end
