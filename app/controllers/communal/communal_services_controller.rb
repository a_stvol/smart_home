class Communal::CommunalServicesController < ApplicationController

  load_and_authorize_resource
  before_action :set_communal_service, only: [:show, :edit, :update, :destroy]

  # GET /communal_services
  # GET /communal_services.json
  def index
    @communal_services = CommunalService.order(:end_period => 'DESC').paginate(:page => params[:page], :per_page => 10)
  end

  # GET /communal_services/1
  # GET /communal_services/1.json
  def show
    @electrics = @communal_service.electrics.all
    @hotwaters = @communal_service.hotwaters.all
    @coldwaters = @communal_service.coldwaters.all
    @electrics_sum = 0
    @hotwaters_sum = 0
    @coldwaters_sum = 0
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Оплата коммунальных услуг " + (l @communal_service.begin_period).to_s + "-" + (l @communal_service.end_period).to_s, encoding: "UTF-8", layout: "communal_pdf", page_size: "A4"
      end
    end
  end

  # GET /communal_services/new
  def new
    @communal_service = CommunalService.new
    @last_communal = CommunalService.order(:end_period => 'DESC').first
    if @last_communal
      @communal_service.begin_period = @last_communal.end_period
      @communal_service.end_period = @communal_service.begin_period + 1.month
    end
  end

  # GET /communal_services/1/edit
  def edit
  end

  # POST /communal_services
  # POST /communal_services.json
  def create
    @communal_service = CommunalService.new(communal_service_params)

    respond_to do |format|
      if @communal_service.save
        format.html { redirect_to @communal_service, notice: 'Период успешно добавлен.' }
        format.json { render :show, status: :created, location: @communal_service }
      else
        format.html { render :new }
        format.json { render json: @communal_service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /communal_services/1
  # PATCH/PUT /communal_services/1.json
  def update
    respond_to do |format|
      if @communal_service.update(communal_service_params)
        format.html { redirect_to @communal_service, notice: 'Период успешно обновлен.' }
        format.json { render :show, status: :ok, location: @communal_service }
      else
        format.html { render :edit }
        format.json { render json: @communal_service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /communal_services/1
  # DELETE /communal_services/1.json
  def destroy
    @communal_service.destroy
    respond_to do |format|
      format.html { redirect_to communal_services_url, notice: 'Период успешно удален.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_communal_service
      @communal_service = CommunalService.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def communal_service_params
      params.require(:communal_service).permit(:begin_period, :end_period)
    end
end
