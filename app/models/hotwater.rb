class Hotwater < ApplicationRecord
	include CommunalAmount

	belongs_to :communal_service

	validates :last_value, presence: true, :numericality => {:greater_than_or_equal_to => 0}
	validates :current_value, presence: true, :numericality => {:greater_than_or_equal_to => 0}
	validates :price, presence: true, :format => { :with => /\A\d+(?:\.\d{0,2})?\z/ }, :numericality => {:greater_than => 0, :less_than => 1000}
	

	after_save :set_communal_amount
	after_destroy :set_communal_amount

end
