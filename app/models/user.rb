class User < ApplicationRecord

	has_and_belongs_to_many :roles
	
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         #:registerable,
         #:recoverable,
         #:rememberable,
         :trackable,
         #:validatable,
         :lockable,
         :timeoutable


  validates :login, presence: true, length: {maximum: 255}, uniqueness: { case_sensitive: false }, format: { with: /\A[a-zA-Z0-9]*\z/, message: "Может содержать только буквы и числа." }
  validates :name, presence: true, length: {maximum: 255}
  validates :email, presence: true, uniqueness: { case_sensitive: false }, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }

  
  attr_accessor :role #:active, :super_admin, :admin, :order_manager, :text_manager

    def role?(role)
      if self.roles.find_by(name: role)
        true
      else
        false
      end
    end

end
