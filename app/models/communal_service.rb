class CommunalService < ApplicationRecord

	has_many :electrics, dependent: :destroy
	has_many :hotwaters, dependent: :destroy
	has_many :coldwaters, dependent: :destroy

end
