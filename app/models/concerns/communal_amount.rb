module CommunalAmount

	private

	def set_communal_amount

		def get_cost(com_items)
			costs = 0
			com_items.each do |item|
				costs += (item.current_value - item.last_value) * item.price
			end
			return costs
		end

		amount = 0

		amount += get_cost(self.communal_service.electrics)
		amount += get_cost(self.communal_service.hotwaters)
		amount += get_cost(self.communal_service.coldwaters)

		self.communal_service.update(:amount => amount)
		
	end

end