module Communal::CommunalServicesHelper

	def show_price(price)
    number_to_currency(price, :precision => 2, :unit => "руб.", :delimiter => " ", :separator => ".", :format => "%n %u")
  end

  def make_currency(currency)
    number_to_currency(currency, :precision => 2, :delimiter => "", :separator => ".", :format => "%n")
  end


  def make_table_pdf
  	
  	@last_values = ""
  	@current_values = ""
  	@prices = ""
  	@consumption = ""
  	@cost = ""
  	@cost_sum_html = ""
  	@rowspan = 1
  	@result = 0

  	@colspan_ehc = [ (@communal_service.electrics.count if @communal_service.electrics.count > 0) || 1, (@communal_service.hotwaters.count if @communal_service.hotwaters.count >0) || 1, (@communal_service.coldwaters.count if @communal_service.coldwaters.count > 0) || 1 ]

  	@colspan_ehc.each do |v|
  		if v > 1
  			@rowspan = 2
  		end
  	end

    def make_column(items)
      @cost_sum = 0
      if items.any?
        items.each do |item|
          @last_values += ("<td>" + item.last_value.to_s + "</td>")
  		    @current_values += ("<td>" + item.current_value.to_s + "</td>")
  		    @consumption += ("<td>" + (item.current_value - item.last_value).to_s + "</td>")
  		    @prices += ("<td>" + make_currency(item.price).to_s + "</td>")
  		    @cost += ("<td>" + make_currency((item.current_value - item.last_value) * item.price).to_s + "</td>")
  		    @cost_sum += (item.current_value - item.last_value) * item.price
        end
      else
        @last_values += ("<td></td>")
        @current_values += ("<td></td>")
        @consumption += ("<td></td>")
        @prices += ("<td></td>")
        @cost += ("<td></td>")
      end

      @result += @cost_sum
    end

    make_column(@communal_service.electrics)
    @cost_sum_html += ("<td class='after_rowspan' colspan='" + @colspan_ehc[0].to_s + "'>" + make_currency(@cost_sum).to_s + "</td>")
    make_column(@communal_service.hotwaters)
    @cost_sum_html += ("<td colspan='" + @colspan_ehc[1].to_s + "'>" + make_currency(@cost_sum).to_s + "</td>")
    make_column(@communal_service.coldwaters)
    @cost_sum_html += ("<td colspan='" + @colspan_ehc[2].to_s + "'>" + make_currency(@cost_sum).to_s + "</td>")

  	return @last_values, @current_values, @consumption, @prices, @cost, @cost_sum_html, @result, @colspan_ehc, @rowspan
  end
	
end
