class ApplicationMailer < ActionMailer::Base
  default from: 'info@smarthome.monika-net.ru'
  layout 'mailer'
end
