require 'test_helper'

class Communal::CommunalServicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @communal_service = communal_services(:one)
  end

  test "should get index" do
    get communal_services_url
    assert_response :success
  end

  test "should get new" do
    get new_communal_service_url
    assert_response :success
  end

  test "should create communal_service" do
    assert_difference('CommunalService.count') do
      post communal_services_url, params: { communal_service: { period: @communal_service.period } }
    end

    assert_redirected_to communal_service_url(CommunalService.last)
  end

  test "should show communal_service" do
    get communal_service_url(@communal_service)
    assert_response :success
  end

  test "should get edit" do
    get edit_communal_service_url(@communal_service)
    assert_response :success
  end

  test "should update communal_service" do
    patch communal_service_url(@communal_service), params: { communal_service: { period: @communal_service.period } }
    assert_redirected_to communal_service_url(@communal_service)
  end

  test "should destroy communal_service" do
    assert_difference('CommunalService.count', -1) do
      delete communal_service_url(@communal_service)
    end

    assert_redirected_to communal_services_url
  end
end
