require 'test_helper'

class Communal::ElectricsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @electric = electrics(:one)
  end

  test "should get index" do
    get electrics_url
    assert_response :success
  end

  test "should get new" do
    get new_electric_url
    assert_response :success
  end

  test "should create electric" do
    assert_difference('Electric.count') do
      post electrics_url, params: { electric: { current_value: @electric.current_value, last_value: @electric.last_value, price: @electric.price } }
    end

    assert_redirected_to electric_url(Electric.last)
  end

  test "should show electric" do
    get electric_url(@electric)
    assert_response :success
  end

  test "should get edit" do
    get edit_electric_url(@electric)
    assert_response :success
  end

  test "should update electric" do
    patch electric_url(@electric), params: { electric: { current_value: @electric.current_value, last_value: @electric.last_value, price: @electric.price } }
    assert_redirected_to electric_url(@electric)
  end

  test "should destroy electric" do
    assert_difference('Electric.count', -1) do
      delete electric_url(@electric)
    end

    assert_redirected_to electrics_url
  end
end
