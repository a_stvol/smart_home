require 'test_helper'

class Communal::ColdwatersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @coldwater = coldwaters(:one)
  end

  test "should get index" do
    get coldwaters_url
    assert_response :success
  end

  test "should get new" do
    get new_coldwater_url
    assert_response :success
  end

  test "should create coldwater" do
    assert_difference('Coldwater.count') do
      post coldwaters_url, params: { coldwater: { current_value: @coldwater.current_value, last_value: @coldwater.last_value, price: @coldwater.price } }
    end

    assert_redirected_to coldwater_url(Coldwater.last)
  end

  test "should show coldwater" do
    get coldwater_url(@coldwater)
    assert_response :success
  end

  test "should get edit" do
    get edit_coldwater_url(@coldwater)
    assert_response :success
  end

  test "should update coldwater" do
    patch coldwater_url(@coldwater), params: { coldwater: { current_value: @coldwater.current_value, last_value: @coldwater.last_value, price: @coldwater.price } }
    assert_redirected_to coldwater_url(@coldwater)
  end

  test "should destroy coldwater" do
    assert_difference('Coldwater.count', -1) do
      delete coldwater_url(@coldwater)
    end

    assert_redirected_to coldwaters_url
  end
end
