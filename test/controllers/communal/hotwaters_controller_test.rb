require 'test_helper'

class Communal::HotwatersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hotwater = hotwaters(:one)
  end

  test "should get index" do
    get hotwaters_url
    assert_response :success
  end

  test "should get new" do
    get new_hotwater_url
    assert_response :success
  end

  test "should create hotwater" do
    assert_difference('Hotwater.count') do
      post hotwaters_url, params: { hotwater: { current_value: @hotwater.current_value, last_value: @hotwater.last_value, price: @hotwater.price } }
    end

    assert_redirected_to hotwater_url(Hotwater.last)
  end

  test "should show hotwater" do
    get hotwater_url(@hotwater)
    assert_response :success
  end

  test "should get edit" do
    get edit_hotwater_url(@hotwater)
    assert_response :success
  end

  test "should update hotwater" do
    patch hotwater_url(@hotwater), params: { hotwater: { current_value: @hotwater.current_value, last_value: @hotwater.last_value, price: @hotwater.price } }
    assert_redirected_to hotwater_url(@hotwater)
  end

  test "should destroy hotwater" do
    assert_difference('Hotwater.count', -1) do
      delete hotwater_url(@hotwater)
    end

    assert_redirected_to hotwaters_url
  end
end
